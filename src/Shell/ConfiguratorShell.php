<?php
namespace Recaptcha\Shell;

use Cake\Console\Shell;
use Cake\Core\Plugin;
use Cake\Filesystem\Folder;

/**
 * Realiza la instalación de la aplicación
 */
class ConfiguratorShell extends Shell
{

/**
 * Crea el fichero de configuración por defecto del plugin
 * 
 * @return void
 */
  public function main()
  {
    $data = "<?php
	\$config = [
    	  'Recaptcha' => [
          // Register API keys at https://www.google.com/recaptcha/admin
          'siteKey' => '6Lfx2QETAAAAAG1Z0V8GB0ZDJ9Rw3SzqE0dO571X',
          'secret' => '6Lfx2QETAAAAADlSGRswyYIuQmowOeJAv2cqvAES',
          // reCAPTCHA supported 40+ languages listed here: https://developers.google.com/recaptcha/docs/language
          'defaultLang' => 'es',
          // either light or dark
          'defaultTheme' => 'light',
          // either image or audio
          'defaultType' => 'image',
         ]
        ];";

    // Creación de ficheros de configuración
    $this->writefile( $data );
  }

  public function writefile( $data )
  {
    $file = fopen( "config/recaptcha.php", "wb");
    fwrite( $file, $data);
    fclose( $file);
    $this->out( 'Configuración para el plugin Recaptcha creada');
  }

}
